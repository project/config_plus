About this Module
-----------------

Provides the following features:
- a service for easily installing new configuration added to a module as if it was installed by the Drupal's own config installer during module installation
- a constraint preventing the non-proper creation of configuration entities
- a fix on installation for wrongly installed configuration entities that miss their UUIDs and this could create all different kind of issues

Usage
-----------------
- in order to install a new config shipped with a module just do the following in a hook_update_N(): \Drupal::service('config_plus.config_installer')->installConfig('module', 'my_module', 'field.field.node.my_node_type.my_new_field');

Notes
-----------------
- The constraint for checking the proper installation of a config entity simply relies on the fact that the UUID in such cases is not set. A perfect check would have to follow if for every config entity there was both a config crud event fired and a hook_entity_insert() or hook_entity_update() invoked, which we might consider adding in a later release.
- Further the constraint does not really prevent the in-proper creation of configuration entities since it fires after the saving, but by throwing an exception and saving an error log message should be enough to make the developer aware of doing something wrong.
