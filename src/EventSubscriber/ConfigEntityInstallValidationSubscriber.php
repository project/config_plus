<?php

namespace Drupal\config_plus\EventSubscriber;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\ConfigException;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * A subscriber checking that a config entity is installed properly.
 */
class ConfigEntityInstallValidationSubscriber implements EventSubscriberInterface {

  /**
   * The configuration manager.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * CaptchaCachedSettingsSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   The configuration manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigManagerInterface $config_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->configManager = $config_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Validates that new configs are created through the config entity API.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The Event to process.
   *
   * @throws \Drupal\Core\Config\ConfigException
   *   If the config is not saved properly through the config entity API.
   */
  public function onSave(ConfigCrudEvent $event) {
    $config = $event->getConfig();
    $name = $config->getName();
    $entity_type_id = $this->configManager->getEntityTypeIdByName($name);
    if ($entity_type_id) {
      /** @var \Drupal\Core\Config\Entity\ConfigEntityTypeInterface $entity_type */
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
      if ($entity_type->hasKey('uuid')) {
        $uuid_key = $entity_type->getKey('uuid');
        if (empty($config->get($uuid_key))) {
          // Saving a config entity through the config factory instead through
          // the config entity API results into a config entity without an UUID.
          // We do not add details aout the missing UUID, but rather that we
          // detected an incorrect save.
          throw new ConfigException('Please use the config entity API to install the new config entity "' . $name . '".' );
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::SAVE][] = ['onSave'];
    return $events;
  }

}
