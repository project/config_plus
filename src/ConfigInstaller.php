<?php

namespace Drupal\config_plus;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Config\Entity\ConfigDependencyManager;
use Drupal\Core\Config\FileStorage;
use Drupal\Core\Config\InstallStorage;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\Yaml\Yaml;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ConfigInstaller {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The typed configuration manager.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfig;

  /**
   * The configuration manager.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The extension path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The config rewriter.
   *
   * @var \Drupal\config_rewrite\ConfigRewriter|NULL
   */
  protected $configRewriter;

  /**
   * A logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $configRewriteLogger;

  /**
   * Constructs the configuration installer.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Config\StorageInterface $active_storage
   *   The active configuration storage.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config
   *   The typed configuration manager.
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   The configuration manager.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   *   The extension path resolver.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   * @param \Drupal\config_rewrite\ConfigRewriter|NULL $config_rewriter
   *   (Optional) The config rewriter service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface|NULL $config_rewrite_logger
   *   (Optional) The config rewriter logger channel.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StorageInterface $active_storage, TypedConfigManagerInterface $typed_config, ConfigManagerInterface $config_manager, EventDispatcherInterface $event_dispatcher, ExtensionPathResolver $extension_path_resolver, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, FileSystemInterface $file_system, $config_rewriter, $config_rewrite_logger) {
    $this->configFactory = $config_factory;
    $this->typedConfig = $typed_config;
    $this->eventDispatcher = $event_dispatcher;
    $this->configStorage = $active_storage;
    $this->configManager = $config_manager;
    $this->extensionPathResolver = $extension_path_resolver;
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->fileSystem = $file_system;
    $this->configRewriter = $config_rewriter;
    $this->configRewriteLogger = $config_rewrite_logger;
  }

  /**
   * Installs new config.
   *
   * Currently, only config entities in the default collection are supported.
   *
   * @param $type
   *   The extension type.
   * @param $extension_name
   *   The extension name.
   * @param $prefix
   *   (optional) The prefix to search for. If omitted, all configuration object
   *   names that exist are returned.
   *
   * @return void
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function installConfig($type, $extension_name, $prefix = '') {
    $collection = StorageInterface::DEFAULT_COLLECTION;

    $module_path = $this->extensionPathResolver->getPath($type, $extension_name);
    $config_dir_paths = [
      $module_path  . '/' . InstallStorage::CONFIG_INSTALL_DIRECTORY,
      $module_path  . '/' . InstallStorage::CONFIG_OPTIONAL_DIRECTORY,
    ];
    $config_to_create = [];
    foreach ($config_dir_paths as $config_dir_path) {
      $storage = new FileStorage($config_dir_path, StorageInterface::DEFAULT_COLLECTION);
      $config_to_create += $storage->readMultiple($storage->listAll($prefix));
    }

    $dependency_manager = new ConfigDependencyManager();
    $config_names = $dependency_manager
      ->setData($config_to_create)
      ->sortAll();

    foreach ($config_names as $name) {
      $new_config = new Config($name, $this->configStorage, $this->eventDispatcher, $this->typedConfig);

      if ($config_to_create[$name] !== FALSE) {
        $new_config->setData($config_to_create[$name]);
        // Add a hash to configuration created through the installer so it is
        // possible to know if the configuration was created by installing an
        // extension and to track which version of the default config was used.
        if ($collection == StorageInterface::DEFAULT_COLLECTION) {
          $new_config->set('_core.default_config_hash', Crypt::hashBase64(serialize($config_to_create[$name])));
        }
      }
      $entity_type_id = $this->configManager->getEntityTypeIdByName($name);
      /** @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $entity_storage */
      $entity_storage = $this->entityTypeManager->getStorage($entity_type_id);

      $id = $entity_storage::getIDFromConfigName($name, $entity_storage->getEntityType()->getConfigPrefix());
      // Install only new config.
      if (!$this->configStorage->exists($name)) {
        // Add the site configured langcode to the config data.
        $site_langcode = $this->configFactory->get('system.site')->get('langcode');
        $langcode_key = $entity_storage->getEntityType()->getKey('langcode');
        $new_config_data = $new_config->get() + [$langcode_key => $site_langcode];

        $entity = $entity_storage->createFromStorageRecord($new_config_data);
        if ($entity->isInstallable()) {
          $entity->trustData()->save();
          if ($id !== $entity->id()) {
            trigger_error(sprintf('The configuration name "%s" does not match the ID "%s"', $name, $entity->id()), E_USER_WARNING);
          }
        }
      }
    }

    // Integrate with config_rewrite.
    $this->rewriteConfigs($config_names);
  }

  /**
   * Rewrite newly installed configuration from other modules.
   *
   * @param array $config_names
   *  The config names to apply the available config rewrites from other
   *  modules.
   *
   * @return void
   */
  protected function rewriteConfigs(array $config_names) {
    // Config rewrite only if the config_rewrite module is enabled.
    if ($this->configRewriter === NULL) {
      return;
    }

    foreach ($this->moduleHandler->getModuleList() as $extension) {
      // Config rewrites are stored in 'modulename/config/rewrite'.
      $rewrite_dir = $extension->getPath() . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'rewrite';

      if (file_exists($rewrite_dir) && $files = $this->fileSystem->scanDirectory($rewrite_dir, '/^.*\.yml$/i', ['recurse' => FALSE])) {
        foreach ($files as $file) {
          // Process only the configs we've just installed.
          if (!in_array($file->name, $config_names, TRUE)) {
            continue;
          }

          // Parse the rewrites and retrieve the original config.
          $rewrite = Yaml::parse(file_get_contents($rewrite_dir . DIRECTORY_SEPARATOR . $file->name . '.yml'));

          $config = $this->configFactory->getEditable($file->name);
          $original_data = $config->getRawData();

          $rewrite = $this->configRewriter->rewriteConfig($original_data, $rewrite, $file->name, $extension->getName());

          // Unset 'config_rewrite' key before saving rewritten values.
          if (isset($rewrite['config_rewrite'])) {
            unset($rewrite['config_rewrite']);
          }

          // Retain the original 'uuid' and '_core' keys if it's not explicitly
          // asked to rewrite them.
          if (isset($rewrite['config_rewrite_uuids'])) {
            unset($rewrite['config_rewrite_uuids']);
          }
          else {
            foreach (['_core', 'uuid'] as $key) {
              if (isset($original_data[$key])) {
                $rewrite[$key] = $original_data[$key];
              }
            }
          }

          // Save the rewritten configuration data.
          $result = $config->setData($rewrite)->save() ? 'rewritten' : 'not rewritten';

          // Log a message indicating whether the config was rewritten or not.
          $this->configRewriteLogger->notice('@config @result by @module', ['@config' => $file->name, '@result' => $result, '@module' => $extension->getName()]);
        }
      }
    }
  }

}
